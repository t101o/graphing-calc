// @author Teodors Drazlovskis
// @date-last-modified 09.01.2022.

const FUNCTION_PARSER = new math.parser();
const PRECISION = 500;
const PLANE_GRAPH_PADDING = 5;
const SCALE_LIMIT = 50;
const FORMULA_COUNT_MAX = 5;
const FORMULA_COUNT_MIN = 1;
const ZOOM_INITIAL = 10.0;
const FORMULA_FIRST_LETTER = "f";
const ZOOM_RATE = 0.5;
const MOUSE_LABEL_MARGIN = 10;
var FORMULA_COUNT_CURRENT = FORMULA_COUNT_MIN;

var plane;
var isMouseDown = false;
var previousMouseCoord = new Point(0,0);

function sleep(milliseconds) {  
    return new Promise(resolve => setTimeout(resolve, milliseconds));  
}  

function getNextFormulaLetter(letter) {
    return String.fromCharCode(letter.charCodeAt(0) + 1);
}

async function movePlane(mousePointer) {
    plane.offset.x += previousMouseCoord.x - mousePointer.clientX;
    plane.offset.y += previousMouseCoord.y - mousePointer.clientY;

    plane.origin.setCoordinates(
        plane.canvas.width / 2 - plane.offset.x,
        plane.canvas.height / 2 - plane.offset.y);

    previousMouseCoord.setCoordinates(
        mousePointer.clientX,
        mousePointer.clientY);

    await sleep(50);
    plane.calculateSectionLengths();
    plane.redraw();
}

function lockScroll() {
    var lockX = window.scrollX;
    var lockY = window.scrollY;

    function lock() {
        window.scrollTo(lockX,lockY);
        return false;
    }

    window.addEventListener("scroll",lock,false);

    return {
        stop: function(){
            window.removeEventListener("scroll",lock,false)
        }
    }
}

function formatCoorinateString(x, y) {
    return `(${(x >= 0) ? " " : ""}${x.toFixed(2)}${(y >= 0) ? ", " : ","}${y.toFixed(2)})`;
}

function setMouseLabelText(label, x, y) {
    label.innerText = formatCoorinateString(x, y);
}

function showDOM(dom) {
    dom.setAttribute("style", "visibility: visible")
}

function moveMouseLabel(label, x, y) {
    let margin = MOUSE_LABEL_MARGIN;
    label.style.left = x + margin;
    label.style.top = y + margin;
}

function hideDOM(dom) {
    dom.setAttribute("style", "visibility: hidden");
}

function calculateCoordinates(mousePointer) {
    let canvasRect = plane.canvas.getBoundingClientRect();
    let canvasX = Math.round(mousePointer.clientX - canvasRect.left);
    let canvasY = Math.round(mousePointer.clientY - canvasRect.top);

    let x = Math.round(((canvasX - plane.origin.x) / plane.mathSectionLength) * 100) / 100;
    let y = Math.round(((plane.origin.y - canvasY) / plane.mathSectionLength) * 100) / 100;

    return new Point(x, y);
}

// AFTER PAGE HAS LOADED
window.onload = function() {

    let addButton = document.getElementById("addFormula");
    let centerButton = document.getElementById("goToCenter");
    let coordinates = document.getElementById("coordinates");

    plane = new Plane("graphingPlane", ZOOM_INITIAL);
    plane.recalculateParameters();
    plane.redraw();

    window.onresize = function() {
        plane.recalculateParameters();
        plane.redraw();
    }

    addButton.onclick = function() {
        let lastFormula;
        
        if (FORMULA_COUNT_CURRENT < FORMULA_COUNT_MAX) {
            let formula = plane.getFirstRemovedFormula();

            if (formula != undefined) {
                formula.showInputField();
            } else {
                lastFormula = plane.formulaList[plane.formulaList.length - 1];
                formula = new Formula(getNextFormulaLetter(lastFormula.letter));
                plane.addFormula(formula);
            }
            FORMULA_COUNT_CURRENT += 1;
        }

        if (FORMULA_COUNT_CURRENT === FORMULA_COUNT_MAX) {
            hideDOM(addButton);
        }   
    }

    centerButton.onclick = function() {
        plane.offset.setCoordinates(0, 0);
        plane.redraw();
    }

    plane.canvas.onmousemove = function(mousePointer) {
        showDOM(coordinates);

        if (isMouseDown) {
            movePlane(mousePointer);
        }

        let mousePointerCoordinates = calculateCoordinates(mousePointer);

        setMouseLabelText(coordinates, mousePointerCoordinates.x, mousePointerCoordinates.y);
        moveMouseLabel(coordinates, mousePointer.pageX, mousePointer.pageY);
    }

    plane.canvas.onmouseout = function() {
        hideDOM(coordinates);
    }

    plane.canvas.onmousedown = function(mousePointer) {
        isMouseDown = true;
        previousMouseCoord.setCoordinates(mousePointer.clientX, mousePointer.clientY);
    }

    plane.canvas.onmouseup = function() {
        isMouseDown = false;
    }

    plane.canvas.onwheel = async function(mouse) {
        let scrollLocker = lockScroll();
        let previousMathSectionLength = plane.mathSectionLength;

        if (mouse.deltaY < 0) {
            // Zoom in
            plane.zoom = parseFloat(plane.zoom) - ZOOM_RATE;
            if (plane.zoom < 1) plane.zoom = 1;
        } else {
            // Zoom out
            plane.zoom = parseFloat(plane.zoom) + ZOOM_RATE;
        }

        plane.calculateSectionLengths();
        plane.offset.x *= plane.mathSectionLength / previousMathSectionLength;
        plane.offset.y *= plane.mathSectionLength / previousMathSectionLength;

        await sleep(50);

        plane.redraw();
        let mousePointerCoordinates = calculateCoordinates(mouse);
        setMouseLabelText(coordinates, mousePointerCoordinates.x, mousePointerCoordinates.y);
        scrollLocker.stop();
    }
};