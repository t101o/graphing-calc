// @author Teodors Drazlovskis
// @date-last-modified 09.01.2022.

class Formula {
    container = document.getElementById("formulaContainer");
    constructor(letter) {
        this.letter = letter;
        this.formula = letter + "(x) = ";
        this.isRemoved = false;
        this.createDOM();
    }

    createDOM() {
        this.label = document.createElement("label");
        this.input = document.createElement("input");
        this.button = document.createElement("button");

        this.input.setAttribute("id", "formulaTextBox");
        this.input.setAttribute("type", "text");
        this.input.setAttribute("value", "");
        this.input.addEventListener("change", this.onChange.bind(this));

        this.label.setAttribute("for", "formulaTextBox");
        this.label.setAttribute("id", "formulaLabel");
        this.label.innerText = this.formula;

        this.button.setAttribute("id", "formulaRemoveButton");
        this.button.innerText = "-";
        this.button.addEventListener("click", this.removeInputField.bind(this));

        this.innerContainer = document.createElement("div");
        this.innerContainer.setAttribute("class", "formulaInnerContainer");
        this.innerContainer.appendChild(this.label);
        this.innerContainer.appendChild(this.input);
        this.innerContainer.appendChild(this.button);

        this.container.appendChild(this.innerContainer);
    }

    onChange() {
        try {
            this.input.value = math.simplify(this.input.value);
            FUNCTION_PARSER.evaluate(this.formula + this.input.value);
            this.input.style.borderColor = "gray";
            if (this.input.value === "undefined") {
                this.input.value = "";
            }
            plane.redraw();
        } catch {
            console.log("Failed" + this.formula + this.input.value);
            this.input.style.borderColor = "red";
            console.log(this.input.style.backgroundColor);
        }
    }

    removeInputField() {

        FUNCTION_PARSER.remove(this.letter);
        if (FORMULA_COUNT_CURRENT === FORMULA_COUNT_MIN) {
            this.input.value = "";
        } else {
            this.container.removeChild(this.innerContainer);
            this.input.value = "";
            this.isRemoved = true;
    
            if (FORMULA_COUNT_CURRENT === FORMULA_COUNT_MAX) {
                let addButton = document.getElementById("addFormula");
                addButton.setAttribute("style", "visibility: visible");
            }
    
            FORMULA_COUNT_CURRENT -= 1;
        }
        plane.redraw();
    }

    showInputField() {
        this.container.appendChild(this.innerContainer);
        this.isRemoved = false;
    }

}