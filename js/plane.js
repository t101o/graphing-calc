// @author Teodors Drazlovskis
// @date-last-modified 09.01.2022.

class Plane {
    formulaList = [];
    origin = new Point(0,0);
    offset = new Point(0,0);

    constructor(canvasId, zoom) {
        this.canvas = document.getElementById(canvasId);
        this.resetDimensions();
        this.context = this.canvas.getContext("2d");
        this.zoom = parseFloat(zoom);
        this.formula = new Formula(FORMULA_FIRST_LETTER);
        this.addFormula(this.formula);
        this.calculateSectionLengths();
    }

    clearPlane() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }

    drawOrigin() {
        this.origin.setCoordinates(this.canvas.width / 2 - this.offset.x,
            this.canvas.height / 2 - this.offset.y);
        
        this.context.strokeStyle = '#000000';

        this.context.beginPath();
        this.context.moveTo(this.origin.x, 0);
        this.context.lineTo(this.origin.x, this.canvas.height);

        this.context.moveTo(0, this.origin.y);
        this.context.lineTo(this.canvas.width, this.origin.y);
        this.context.stroke();
        this.context.closePath();
    }

    drawGrid() {
        this.context.strokeStyle = '#D3D3D3';

        // Draw right side from origin
        for (let x = 1; this.origin.x + this.displaySectionLength * x < this.canvas.width; x += 1) {
            this.context.beginPath();
            this.context.moveTo(this.origin.x + this.displaySectionLength * x, 0);
            this.context.lineTo(this.origin.x + this.displaySectionLength * x, this.canvas.height);
            this.context.stroke();
            this.context.closePath();
        }

        // Draw left side from origin
        for (let x = 1; this.origin.x - this.displaySectionLength * x > 0; x += 1) {
            this.context.beginPath();
            this.context.moveTo(this.origin.x - this.displaySectionLength * x, 0);
            this.context.lineTo(this.origin.x - this.displaySectionLength * x, this.canvas.height);
            this.context.stroke();
            this.context.closePath();
        }

        // Draw top from origin
        for (let y = 1; this.origin.y - this.displaySectionLength * y > 0; y += 1) {
            this.context.beginPath();
            this.context.moveTo(0, this.origin.y - this.displaySectionLength * y);
            this.context.lineTo(this.canvas.width, this.origin.y - this.displaySectionLength * y);
            this.context.stroke();
            this.context.closePath();
        }

        // Draw bottom from origin
        for (let y = 1; this.origin.y + this.displaySectionLength * y < this.canvas.height; y += 1) {
            this.context.beginPath();
            this.context.moveTo(0, this.origin.y + this.displaySectionLength * y);
            this.context.lineTo(this.canvas.width, this.origin.y + this.displaySectionLength * y);
            this.context.stroke();
            this.context.closePath();
        }
        this.context.strokeStyle = '#000000';
    }

    resetDimensions() {
        let canvasParent = this.canvas.parentElement;
        let canvasParentHeightEven = canvasParent.clientHeight - (canvasParent.clientHeight % 2);
        let canvasParentWidthEven = canvasParent.clientWidth - (canvasParent.clientWidth % 2);
        this.canvas.height = canvasParentHeightEven;
        this.canvas.width = canvasParentWidthEven;
        this.origin.setCoordinates(this.canvas.width / 2 - this.offset.x,
            this.canvas.height / 2 - this.offset.y);
    }
    
    drawGraphs() {
        let isOutOfBounds = false;
        let xMax = (this.canvas.width - this.origin.x) / this.mathSectionLength;
        let xMin = -this.origin.x / this.mathSectionLength;

        let yMin = (this.origin.y - this.canvas.height) / this.mathSectionLength;
        let yMax = this.origin.y / this.mathSectionLength;

        for (let formula of this.formulaList) {
            
            if (!(FUNCTION_PARSER.get(formula.letter) === undefined)) {
                
                this.context.beginPath();

                for (let i = 0; i <= PRECISION; i += 0.2) {
                    if (isOutOfBounds) {
                        this.context.stroke();
                        this.context.closePath();
                        this.context.beginPath();
                        isOutOfBounds = false;
                    }
                    let percentX = i / (PRECISION - 1);
                    let mathX = percentX * (xMax - xMin) + xMin;
                    let mathY;
                    try {
                        mathY = FUNCTION_PARSER.evaluate(formula.letter + '(' + mathX + ')');
                    } catch {
                        // Failed drawing
                        formula.input.style.borderColor = "red";
                        this.context.stroke();
                        this.context.closePath();
                        this.context.beginPath();
                        continue; 
                    }
                    let percentY = (mathY - yMin) / (yMax - yMin);

                    let x = percentX * this.canvas.width;
                    let y = (this.canvas.height) - percentY * this.canvas.height;

                    if (x <= -PLANE_GRAPH_PADDING * this.displaySectionLength) {
                        x = -PLANE_GRAPH_PADDING * this.displaySectionLength;
                        isOutOfBounds = true;
                    } else if (x >= this.canvas.width + PLANE_GRAPH_PADDING * this.displaySectionLength) {
                        x = this.canvas.width + PLANE_GRAPH_PADDING * this.displaySectionLength;
                        isOutOfBounds = true;
                    }

                    if (y <= -PLANE_GRAPH_PADDING * this.displaySectionLength) {
                        y = -PLANE_GRAPH_PADDING * this.displaySectionLength;
                        isOutOfBounds = true;
                    } else if (y >= this.canvas.height + PLANE_GRAPH_PADDING * this.displaySectionLength) {
                        y = this.canvas.height + PLANE_GRAPH_PADDING * this.displaySectionLength;
                        isOutOfBounds = true;
                    }

                    this.context.lineTo(x, y);
                }

                this.context.stroke();
                this.context.closePath();
            }
        }
    }
    
    addFormula(formula) {
        this.formulaList.push(formula);
    }

    calculateSectionLengths() {
        this.mathSectionLength = Math.min(this.canvas.height, this.canvas.width) / (2 * this.zoom);
        this.displaySectionLength = this.mathSectionLength;
    }

    getFirstRemovedFormula() {
        for (let i = 0; i < this.formulaList.length; i++) {
            if (this.formulaList[i].isRemoved) {
                return this.formulaList[i];
            }
        }
        return undefined;
    }

    recalculateParameters() {
        this.resetDimensions();
        this.calculateSectionLengths();
    }

    redraw() {
        this.clearPlane();
        this.drawGrid();
        this.drawOrigin();
        this.drawGraphs();
    }

}
