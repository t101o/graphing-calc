// @author Teodors Drazlovskis
// @date-last-modified 09.01.2022.

class Point {
    constructor(x, y) {
        this.x = parseFloat(x);
        this.y = parseFloat(y);
    }

    setCoordinates(x, y) {
        this.x = x;
        this.y = y;
    }
}